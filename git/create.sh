#!/bin/sh
DIRECTORY=`pwd`
PACKAGE=${DIRECTORY##*/}
PARENT_DIR=${DIRECTORY%/*}
DISTRO=${PARENT_DIR##*/}

docker build --no-cache --rm=true -t petervanderdoes/buildenv-${DISTRO}:${PACKAGE} .
